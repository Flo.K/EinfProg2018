package SB2;


public class Room
{
    String name;
    int height; // in cm deklarieren int-Wert
    int width; // in cm
    int length; // in cm
    boolean hasWindow; // true/false

    public Room()
    {
        name = "";
        height = 0;
        width= 0 ;
        length= 0;
        hasWindow = false;
    }

    public Room(String name, int height, int width, int length, boolean hasWindow)
    {
        this.name = name;
        this.height = height;
        this.width = width;
        this.length = length;
        this.hasWindow = hasWindow;

    }

    static int calcArea(int length, int width, int height)
    {
        int groundAndCeiling;
        int wall;
        wall = (2*(length*height)+2*(width*height)); // 4 Wände brechnen
        groundAndCeiling = 2*(length*width); // Decke und Boden berechnen
        return wall+groundAndCeiling;

    }

    static int calcVolume(int length, int width, int height)
    {
        int volume;
        volume = length * width * height;
        return volume;

    }

    int getArea()
    {
        return Room.calcArea(length, width, height);
    }

    int getVolume()
    {
        return Room.calcVolume(length, width, height);
    }

    String getStatus()
    {
        String sReturn = "name: " + name + "; length: " + length + "cm; width: " + width + "cm; height: " + height + "cm; hasWindow: " + hasWindow ;

        return sReturn;
    }


}