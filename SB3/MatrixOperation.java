package SB3;


public class MatrixOperation
{

//##################################################################################################################################
//######################################## Funktion add, statisch ##################################################################
//##################################################################################################################################

   static int[][] add (int[][] first, int[][] second)
   {
        int counterFirstOne = 0 ;
        int counterFirstTwo = 0 ;
        int counterSecondOne = 0;
        int counterSecondTwo = 0;        

        //Zeilen auslesen first Anzahl Dimensionen
        for(int row = 0; row < first.length; row++) {
            counterFirstOne++;

            //Spalten auslesen first Anzahl der gesammten enthaltenden Zahlen
            for(int column = 0; column < first[row].length;column++) {
                counterFirstTwo++;
            }
        }

        //Zeilen auslesen Second
        for(int row = 0; row < second.length; row++) {
            counterSecondOne++;

            //Spalten auslesen second
            for(int column = 0; column < second[row].length;column++) {
                counterSecondTwo++;
            }
        }

        

        // prüfen der übergeben Arrays auf richtige Dimension und Anzahl der Zahlen
        if(counterFirstOne !=0 && counterSecondOne !=0 && counterFirstOne == counterSecondOne && counterFirstTwo == counterSecondTwo && counterFirstTwo !=0 && counterSecondTwo !=0) { 
           
            //result[row][column] result jetzt variabel
            int[][] result= new int[counterFirstOne][first[0].length];

            //Zeilen auslesen first Anzahl Dimensionen 
            for(int row = 0; row < first.length; row++) {  
                 
                 //Spalten auslesen first Anzahl der gesammten enthaltenden Zahlen
                 for(int column = 0; column < first[row].length;column++) {
                      //fuege Ergebnisse in result ein
                     result [row] [column] = first[row] [column] + second [row] [column]; 
                     }
            }

            return result;  
            
        }

        else {
            return null;
        }
   }

//##################################################################################################################################
//######################################## Funktion multiply, statisch #############################################################
//##################################################################################################################################

    static int [] [] multiply (int[][] first, int [][] second)
   {
    int counterFirstOne = 0 ;
    int counterFirstTwo = 0 ;
    int counterSecondOne = 0;
    int counterSecondTwo = 0;         

    //Zeilen auslesen first Anzahl Dimensionen
    for(int row = 0; row < first.length; row++) {
        counterFirstOne++;

        //Spalten auslesen first Anzahl der gesammten enthaltenden Zahlen
        for(int column = 0; column < first[row].length;column++) {
            if (first[row].length == first[1].length){
                counterFirstTwo++;
            }

            else {
                return null;
            }
            
        }
    }

    //Zeilen auslesen Second
    for(int row = 0; row < second.length; row++) {
        counterSecondOne++;

        //Spalten auslesen second
        for(int column = 0; column < second[row].length;column++) {
            counterSecondTwo++;
        }
    }

    // prüfen der übergeben Arrays auf richtige Dimension und Anzahl der Zahlen
    if(counterFirstOne >0 && counterSecondOne >0 && counterFirstTwo >0 && counterSecondTwo >0 && first[0].length == second.length){
     
        int rowFirst = first.length;
        int columnFirst = first.length;
        int columnSecond = second[0].length;

        // result besitzt die Zeilen von first und spalten von second
        int[][] result = new int [rowFirst] [columnSecond];

        //Zeile Array first 0
        for(int i=0; i< rowFirst;i++){
            //Spalte Array second
            for(int j=0; j<columnSecond; j++){
                //Spalte Array First
                for(int k=0; k<columnFirst; k++){
                    result[i][j]= result[i][j] + first[i][k]*second[k][j];
                }
            }
        }  

        return result;  
        
    }

    else {
       return null;
    }
    
}     
    

/* public static void main(String[] args)
   {
        int[][] first = {{1,2,4}, {4,5,6}};
        int[][] second = {{1,2,4},{4,5,6}};

       int[][] multiplyresult = multiply(first, second); 

       // System.out.println(addResult[0] [0]);
        //System.out.println(addResult[0] [1]);
       // System.out.println(addResult[0] [2]);
        //System.out.println(addResult[1] [0]);
        //System.out.println(addResult[1] [1]);
        //System.out.println(addResult[1] [2]);
        

       //int[][] multiplyResult = multiply(first,second);
   }   */
}