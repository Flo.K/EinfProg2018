package SB3;
import SB3.Node;

public class MyLinkedList{

    private Node head;
    int list = 0;



//############ get Wert Index ########

    public int get(int index) {

        if(this.isEmpty() == true) {
            return -1;
        }   

        if(index == 0) {

            return this.head.getValue();
        }

        if(index > this.size()){
        return -1;
        
        } 

        else{

            int counter = 0;
            Node currentNode = new Node();
            int buffer = 0;

            while(currentNode.getNext() !=null){
                currentNode = currentNode.getNext();
                counter ++;

                if(counter == index) {
                    return currentNode.getValue();
                }
            }
        }
        return 0;
    }

//########## add am Ende #########

    public void add(int value) {
        Node buffer = new Node(value,null);

        if(isEmpty() == true) {
            this.head = buffer;
            return;
        }

        Node currentNode = this.head;

        while(currentNode.getNext() !=null){
            currentNode = currentNode.getNext();
        }
        currentNode.setNext(buffer);
    }


//########## delete (lösche Erstes Element mit dem Wert value) ########## 
    
    public boolean delete(int value) {

        if(isEmpty() == true) {
            return false;
        }
    //########## value == head? #########

        if(this.head.getValue() == value) {
            if(this.head.getNext() != null) {
                this.head = this.head.getNext();
            } 
            else {
                this.head = null;
            }
            
            return true;
        }
      
        Node currentNode = this.head;
        Node beforeNode;
        Node afterNode;

        while (currentNode.getNext() !=null){                    // solange bis getNext null (bis ende der Schleife)
            if(currentNode.getNext().getValue() == value) {     //blick von momentanen Node auf das Value vom naechsten
                beforeNode = currentNode;                       // der aktuelle Node wird 1 zurück gesetzt
                currentNode = currentNode.getNext();            // und der aktuelle Node wird nächste -> fuer die folgende if-Abfrage

                if(currentNode.getNext() != null){              //wenn der nächste Node des gelöschten Nodes != null ist,
                    afterNode = currentNode.getNext();          // setze den afterNode auf den folgenden des letzten Nodes
                }
                else {
                    afterNode = null;                           //ansonsten setze afterNode null
                }

                beforeNode.setNext(afterNode);                  // dann setze den Nextwert des mittleren Wertes auf den Nachfolger des zu löschenden
                return true;

            }
            else{
                currentNode = currentNode.getNext();            // wenn nicht der gesuchte Wert, dann suche weiter
            }
            
        }

     return false;                                              //wenn Wert nicht da, returne false
    }

//########## size #########

    public int size() {
        if(isEmpty() == true){
            return 0;
        }
        
        int listlength = 1;

        Node currentNode = this.head;

        while(currentNode.getNext() !=null){
            currentNode = currentNode.getNext();
            listlength++;
        }
    return listlength;
    }

//########## Is empty? ########## 

    public boolean isEmpty()
    {
        if(this.head == null)
        {
            return true;
        }
            return false;

    }

//########## First? ########## 

    public int first() {
        if(isEmpty() == true){
            return -1;
        }
        return this.head.getValue();
    }

//########### Last? ########## 

    public int last() {
        if(isEmpty() == true){
            return -1;
        }
        
        int lastNode;

        Node currentNode = this.head;

        while(currentNode.getNext() !=null){
            currentNode = currentNode.getNext();
        }

        lastNode = currentNode.getValue();

        return lastNode;

    }

//########### Add at? ###########

    public boolean addAt(int value, int index) {

        if(isEmpty() == true) {
            return false;
        }

        int counter = 0;
        Node currentNode = this.head;
        Node beforeNode;
        Node afterNode;
        Node bufferNode;

        while(currentNode.getNext() !=null){
            currentNode = currentNode.getNext();
            counter ++;

            if(counter == index) {

                beforeNode = currentNode;
                afterNode = currentNode.getNext();
                currentNode.setValue(value);
                currentNode.setNext(afterNode);
                beforeNode.setNext(currentNode);
                

                return true;


            }
        }
    
    return false;
    }
}