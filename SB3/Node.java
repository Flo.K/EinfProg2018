package SB3;

//################### Einstiegspunkt mit Referenz auf den nächsten Node ################
//################### Wenn kein nächstes Note-Objekt,dann nächstes Objekt Null #########


public class Node {

    private int value ;
    private Node next;

//########## Defaul Constructor #####

    public Node () {
    }


//########## Node mit Werten ##########

    public Node (int value, Node next)
    {
        this.value = value;
        this.next = next;
        
    }

//##############################################################################################
//############################################################################################## 
//######################## Methods ############################################################# 
//############################################################################################## 

//########## Setter Value ########## 
    
    public void setValue(int value) {
            this.value = value;

    }
//########## Getter Value ######### 

    public int getValue() {

        return this.value;
    }
//########## setze nächsten Node ##########
    public void setNext(Node node) {
        this.next = node;

    }
//########## erhalte den Nächsten Node ###########
    public Node getNext() {
        return this.next;
    }
}